FROM alpine:3.8

WORKDIR /app

RUN apk --no-cache --update add python3 openssl ca-certificates  && \
    pip3 install --upgrade pip

COPY checkBucketAccess.py requirements.txt /app/
RUN pip3 install -r /app/requirements.txt 

RUN chmod a+x /app/checkBucketAccess.py


ENTRYPOINT ["/app/checkBucketAccess.py"]

