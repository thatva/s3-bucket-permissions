#!/usr/bin/env python3
import boto3
import argparse
from tabulate import tabulate
from collections import defaultdict



parser = argparse.ArgumentParser(description='Simulates S3 access to a bucket for existing users and roles.')
parser.add_argument('bucketname')
parser.add_argument('--format', dest='tablefmt', default='fancy_grid',
        help='Output format: plain, pipe, jira, fancy_grid, html(anything supported by tabulate)')

args = parser.parse_args()

simulate_actions = ['s3:PutObject', 's3:GetObject', 's3:DeleteObject', 's3:ListBucket','s3:DeleteBucket']

acl_groups = {
    "http://acs.amazonaws.com/groups/global/AllUsers": "Everyone",
    "http://acs.amazonaws.com/groups/global/AuthenticatedUsers": "Authenticated AWS users"
}

permissions = {
    "READ": "Read all",
    "WRITE": "Write all",
    "READ_ACP": "Read bucket permissions",
    "WRITE_ACP": "Write bucket permissions",
    "FULL_CONTROL": "There be dragons"
}

iam = boto3.client('iam')
s3  = boto3.client('s3')
userlist = list()
sourcearns = list()
evalresults = {}

bucketname = args.bucketname

#paginator = iam.get_paginator('list_users')
#for response in paginator.paginate():
#    userlist.append(response['Users'])
userlist = iam.list_users()
rolelist = iam.list_roles()

def check_bucket_permissions(arn,bucket):
    evalresults[arn] = {}
    matched_policies = ''
    bucket_objects_arn = 'arn:aws:s3:::%s/*' % bucket
    results = iam.simulate_principal_policy(
        PolicySourceArn=arn,
        ResourceArns=[bucket_objects_arn],
        ActionNames= simulate_actions
        )

    for result in results['EvaluationResults']:
         matched_policies = ''
         if( 'allowed' in result['EvalDecision'] ):
            for key in result['MatchedStatements']:
               matched_policies += key['SourcePolicyId'] + ' '
         evaluationt = result['EvalDecision'] + '(' + matched_policies + ')'
         evalresults[arn].update({ result['EvalActionName'] : evaluationt })

         
    for key in evalresults[arn]:
        putobject = evalresults[arn]['s3:PutObject']
        getobject = evalresults[arn]['s3:GetObject']
        deleteobject = evalresults[arn]['s3:DeleteObject']
        listbucket = evalresults[arn]['s3:ListBucket']
        deletebucket = evalresults[arn]['s3:DeleteBucket']
    return([arn, getobject, putobject, deleteobject, listbucket, deletebucket])


def check_acl(acl):
    dangerous_grants = defaultdict(list)
    for grant in acl['Grants']:
        grantee = grant['Grantee']
        if grantee["Type"] == "Group" and grantee["URI"] in acl_groups:
            dangerous_grants[grantee["URI"]].append(grant["Permission"])
    return dangerous_grants



bucketperms = []
aclperms = []

acl_results = check_acl(s3.get_bucket_acl(Bucket=bucketname))
for grant in acl_results:
     group = acl_groups[grant]
     global_perms = acl_results[grant]
     global_permissions = [permissions[perm] for perm in global_perms]
     aclperms.append([group, global_permissions])

aclheaders=['ARN','Bucket ACL']
print(tabulate(aclperms, aclheaders, tablefmt=args.tablefmt))

for iamuser in userlist['Users'] + rolelist['Roles']:
    bucketperms.append(check_bucket_permissions(iamuser['Arn'],bucketname))
    
    
polheaders=['ARN','s3:getObject','s3:PutObject','s3:DeleteObject', 's3:ListBucket', 's3:DeleteBucket', 'Global Access']
print(tabulate(bucketperms, polheaders, tablefmt=args.tablefmt))

